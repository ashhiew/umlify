from tests.unittest_shelf import ShelfUnitTests
from tests.unittest_database import DatabaseUnitTests
from tests.extractor_unittest import ExtractorUnitTests

import unittest


def unit_tests():
    the_suite = unittest.TestSuite()

    the_suite.addTest(unittest.makeSuite(ShelfUnitTests))
    the_suite.addTest(unittest.makeSuite(DatabaseUnitTests))
    the_suite.addTest(unittest.makeSuite(ExtractorUnitTests))

    return the_suite


if __name__ == "__main__":
    runner = unittest.TextTestRunner(verbosity=2)

    test_suite = unit_tests()
    runner.run(test_suite)
