from component_viewer import ComponentViewer
from extractor import Extractor
import matplotlib.pyplot as plt
from numpy import arange
from graphviz import Digraph
from subprocess import CalledProcessError
from graphviz import ExecutableNotFound
from os import path, remove, makedirs


class UmlifyComponentViewer(ComponentViewer):
    def __init__(self, input_path=None, output_path=None):
        """
        Testing default values
        >>> cv = UmlifyComponentViewer("./tests/test_class_1.py")
        >>> print(cv.input_path)
        ./tests/test_class_1.py
        >>> print(cv.output_class_diagram)
        class-diagram
        >>> print(cv.output_bar_chart)
        bar-chart
        >>> print(cv.output_pie_chart)
        pie-chart
        >>> print(cv.path_provided)
        True
        >>> print(len(cv.components) > 0)
        True
        >>> print(cv.output_path)
        output/

        """
        super().__init__()
        self.input_path = input_path
        self.output_path = output_path
        self.output_class_diagram = "class-diagram"
        self.output_bar_chart = "bar-chart"
        self.output_pie_chart = "pie-chart"
        self.e = Extractor()
        self.error = ""
        self.path_provided = False

        if input_path:
            self.e.set_file(input_path)
            self.path_provided = True
        if output_path is None:
            self.output_path = 'output/'
        self.components = list(self.e.get_component_dictionary().values())

    def generate_class_diagram(self, output_file_name=None):
        dot = None
        if output_file_name is None:
            output_file_name = self.output_class_diagram

        output_file_name = self.output_path + output_file_name

        try:
            dot = self._initialise_diagram('UML Class Diagram', 'dot')
        except ExecutableNotFound:   # pragma: no cover
            self.error = "Graphviz executable not found, please check it is properly installed."
        except CalledProcessError:   # pragma: no cover
            self.error = "An unexpected error occurred"
        if (len(self.components) > 0) & (dot is not None):
            for comp in self.components:
                dot = self._add_comp_to_diagram(dot, comp)
                dot = self._add_parents_to_diagram(dot, comp)
            dot.render(output_file_name, view=False)
            self.write_dot_to_png(dot_file=output_file_name)
            self._remove_temp_files(output_file_name)
        else:
            print("unable to add components to diagram.")

    def generate_pie_charts(self):
        for comp in self.e.get_component_dictionary().keys():
            self.generate_pie_chart(comp)

    def generate_pie_chart(self, comp_name, output_file_name=None):
        if not path.exists(self.output_path):
            makedirs(self.output_path)

        # ensuring the class name passed in starts with an uppercase letter
        comp_name = comp_name[0].upper() + comp_name[1:]
        if output_file_name is None:
            output_file_name = comp_name + '-' + self.output_pie_chart

        output_file_name = self.output_path + output_file_name

        the_component = self.e.get_component_dictionary().get(comp_name)
        if the_component is not None:
            att_types = self._get_attribute_types(the_component)
            removable = []
            for k, v in att_types.items():
                if v == 0:
                    removable.append(k)
            for r in removable:
                att_types = self._remove_by_key(att_types, r)

            if len(att_types) > 0:
                labels = att_types.keys()
                sizes = att_types.values()
                plt.pie(sizes, labels=labels, autopct='%1.1f%%')
                plt.title("Percentage of variable data types")
                plt.axis('equal')
                plt.savefig(output_file_name+".png")
                plt.gcf().clear()

    def generate_bar_chart(self, output_file_name=None):
        if not path.exists(self.output_path):
            makedirs(self.output_path)
        if output_file_name is None:
            output_file_name = self.output_bar_chart
        bar_width = 0.35
        opacity = 0.8
        objects = tuple(self.e.get_component_dictionary().keys())
        if len(objects) > 0:
            no_of_attributes = []
            no_of_functions = []
            for key in objects:
                component = self.e.get_component_dictionary().get(key)
                no_of_attributes.append(len(component.get_attributes()))
                no_of_functions.append(len(component.get_functions()))
            y_pos = arange(len(objects))

            plt.bar(y_pos, no_of_attributes, bar_width, alpha=opacity, color='b', label='attributes')
            plt.bar(y_pos + bar_width, no_of_functions, bar_width, alpha=opacity, color='g', label='functions')
            plt.xticks(y_pos + bar_width, objects, rotation=90)
            plt.subplots_adjust(bottom=0.50)
            plt.ylabel('Count')
            plt.xlabel('Class Name')
            plt.title('Number of Functions and Attributes per Class')
            plt.legend()

            plt.savefig(self.output_path+output_file_name + ".png")
            plt.gcf().clear()
        else:
            print("unable to generate bar chart, no components found")

    @classmethod
    def _get_attribute_types(cls, component):
        att_types = {"int": 0, "str": 0, "dict": 0, "list": 0, "tuple": 0, "object": 0}
        if component is not None:
            attributes = component.get_attributes()

            if attributes is not None:
                if len(attributes) > 0:
                    for attrib in attributes:
                        for a in attributes[attrib]:
                            if a is not '':
                                att_types[a] = att_types[a] + 1
        return att_types

    @classmethod
    def _initialise_diagram(cls, comment, diagram_format):
        dot = Digraph(comment=comment)
        dot.node_attr['shape'] = "record"
        dot.format = diagram_format
        return dot

    @classmethod
    def _add_comp_to_diagram(cls, dot, comp):
        comp_name = comp.get_name()
        attributes = cls._build_attributes_string(comp.get_attributes())
        functions = cls._build_function_string(comp.get_functions())
        record = cls._build_record_string(comp_name, attributes, functions)
        dot.node(comp_name, record)
        return dot

    @classmethod
    def _add_parents_to_diagram(cls, dot, comp):
        for parent in comp.get_parents():
            dot.edge(parent.get_name(), comp.get_name())
            dot.edge_attr.update(dir="back")
            dot.edge_attr.update(arrowtail='empty')
        return dot

    @classmethod
    def _remove_temp_files(cls, output_file_name):
        if path.exists(output_file_name):
            remove(output_file_name)
        if path.exists(output_file_name + ".dot"):
            remove(output_file_name + ".dot")

    @classmethod
    def _remove_by_key(cls, dictionary, key):
        r = dict(dictionary)
        del r[key]
        return r

    @classmethod
    def _build_record_string(cls, comp_name, attributes, functions):
        record = "{"
        record += "{name} | {attribs} |{functs}".format(name=comp_name, attribs=attributes,
                                                        functs=functions)
        record += "}"
        return record

    @classmethod
    def _build_function_string(cls, functions):
        function_string = ""
        for funct in functions:
            function_string += funct + "\\n"
        return function_string

    @classmethod
    def _build_attributes_string(cls, attributes):
        attribute_string = ""
        for attrib in attributes:
            attribute_string += attrib
            for a in attributes[attrib]:
                attribute_string += " : " + a
            attribute_string += "\\n"
        return attribute_string
