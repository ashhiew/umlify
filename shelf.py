import shelve
import os
from extractor import Extractor

"""This module consists of functions that serializes and de-serializes 
    the data of Component objects and storing them into a database known as a shelf. 
    The shelf is accessed by using keys, just like a dictionary. 
    The shelve module provides object persistence and object serialization by using the pickle module.
"""


class Shelf(object):
    """
    >>> sh = Shelf("tests/test_class_2.py")
    >>> sh.write_shelf("test_shelve")
    Class components of [test_class_2.py] serialized and stored to file successfully
    >>> print(sh.read_shelf("test_shelve"))
    Class Name: Person
    Attribute: name
    Attribute: age
    Function: __init__
    --------------------
    <BLANKLINE>
    >>> sh.write_shelf(None)
    Shelf name not entered
    >>> sh.read_shelf(None)
    Traceback (most recent call last):
        ...
    Exception: File Error: 'None' does not exist!
    """
    def __init__(self, filename):
        self.selected_file = filename
        path, self.selected_key = os.path.split(os.path.basename(filename))

    def write_shelf(self, shelf_name=None):
        """Add component data dictionary to shelf file"""
        e = Extractor()
        e.set_file(self.selected_file)
        dict_item = e.get_component_dictionary()

        if shelf_name is None:
            print("Shelf name not entered")
        else:
            with shelve.open(shelf_name, "c") as shelf:
                try:
                    shelf[self.selected_key] = dict_item
                finally:
                    str_format = "Class components of [{}] serialized and stored to file successfully"
                    print(str_format.format(self.selected_key))

    def read_shelf(self, shelf_name=None):
        """Read component data from shelf file"""
        shelf_dict = []

        # return value
        new_line = "\n"

        try:
            output = ""
            with shelve.open(shelf_name, 'r') as shelf:
                for key in shelf.keys():
                    item = shelf[key]
                    shelf_dict.append(item)
        except (IOError, TypeError):
            raise Exception("File Error: '%s' does not exist!" % shelf_name)

        for dict_item in shelf_dict:
            for class_name, class_data in dict_item.items():
                output += "Class Name: " + class_data.name + new_line
                for attr in class_data.attributes:
                    output += "Attribute: " + attr + new_line
                for func in class_data.functions:
                    output += "Function: " + func + new_line
                for parent in class_data.parents:
                    output += "Parent: " + parent.name + new_line
                output += "-"*20 + new_line

        return output

    @classmethod
    def clear_shelf(cls, shelf_name):
        """Removes all Component objects stored within the file"""
        with shelve.open(shelf_name) as shelf:
                shelf.clear()


if __name__ == "__main__":  # pragma: no cover
    import doctest
    doctest.testmod(verbose=True)
