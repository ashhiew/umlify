import unittest
from extractor import Extractor

"""Ten unit tests to test the extractor.py class, primarily the regex extractions"""


class ExtractorUnitTests(unittest.TestCase):
    """Tests for extractor.py`."""

    def test_class_extraction(self):
        """A class name can be extracted from line of code"""
        # Arrange
        e = Extractor().comp_ext
        expected = 'Marsupial'
        line = "class Marsupial(Mammal):"

        # Act
        actual = e._extract_class(line)

        # Assert
        self.assertEqual([expected], actual)

    def test_set_file(self):
        e = Extractor()
        expected = 11

        e.set_file(".\\tests")
        actual = len(e.get_component_dictionary())

        self.assertEqual(expected, actual)

    def test_set_file_file_not_found_error(self):
        e = Extractor()
        self.assertRaises(FileNotFoundError, e.set_file("test_class_unknown.py"))

    def test_nil_class_extraction(self):
        """A name will not be extracted from line of code where no class present"""
        # Arrange
        e = Extractor().comp_ext
        expected = []
        line = "class(Mammal):"

        # Act
        actual = e._extract_class(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_function_extraction(self):
        """A function name can be extracted from line of code"""
        # Arrange
        e = Extractor().comp_ext
        expected = 'proliferates'
        line = "def proliferates(self):"

        # Act
        actual = e. _extract_functions(line)

        # Assert
        self.assertEqual([expected], actual)

    def test_nil_function_extraction(self):
        """A name will not be extracted from line of code where no function present"""
        # Arrange
        e = Extractor().comp_ext
        expected = []
        line = "    teeth = 'Sharp'"

        # Act
        actual = e._extract_class(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_attribute_extraction(self):
        """An attribute name can be extracted from line of code"""
        # Arrange
        e = Extractor().comp_ext
        expected = 'teeth'
        line = "    self.teeth = 'Sharp'"

        # Act
        actual = e._extract_attributes(line)

        # Assert
        self.assertEqual([expected], actual)

    def test_nil_attribute_extraction(self):
        """A name will not be extracted from line of code where no attribute present"""
        # Arrange
        e = Extractor().comp_ext
        expected = []
        line = "    self.= 'Sharp'"

        # Act
        actual = e._extract_class(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_default_value_extraction(self):
        """An attribute default value can be extracted from line of code"""
        # Arrange
        e = Extractor().datatype_ext
        expected = 'Sharp'
        line = "    self.teeth = Sharp"

        # Act
        actual = e._extract_attribute_defaults(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_default_attribute_zero_length(self):
        # Arrange
        e = Extractor().datatype_ext
        expected = []
        line = "    self.teeth"

        # Act
        actual = e._extract_attribute_defaults(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_nil_default_value_extraction(self):
        """A value will not be extracted from line of code where no default present"""
        # Arrange
        e = Extractor().comp_ext
        expected = []
        line = "    self.teeth ='"

        # Act
        actual = e._extract_class(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_data_type_extraction(self):
        """An attribute data type can be extracted from line of code"""
        # Arrange
        e = Extractor().datatype_ext
        expected = 'str'
        line = "Sharp"

        # Act
        actual = e._extract_attribute_datatypes(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_data_type_object_extraction(self):
        e = Extractor().datatype_ext
        expected = 'obj'
        line = 'TEST()'

        # Act
        actual = e._extract_attribute_datatypes(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_data_type_dict_extraction(self):
        e = Extractor().datatype_ext
        expected = 'dict'
        line = '{test : 12}'

        # Act
        actual = e._extract_attribute_datatypes(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_data_type_list_extraction(self):
        e = Extractor().datatype_ext
        expected = 'list'
        line = '[12, 13, 14]'

        # Act
        actual = e._extract_attribute_datatypes(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_data_type_tuple_extraction(self):
        e = Extractor().datatype_ext
        expected = 'tuple'
        line = '("a", "b")'

        # Act
        actual = e._extract_attribute_datatypes(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_data_type_int_extraction(self):
        e = Extractor().datatype_ext
        expected = 'int'
        line = '5'

        # Act
        actual = e._extract_attribute_datatypes(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_data_type_str_extraction(self):
        e = Extractor().datatype_ext
        expected = 'str'
        line = "'test'"

        # Act
        actual = e._extract_attribute_datatypes(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_unknown_data_type_extraction(self):
        """An error message is raised when an unknown attribute data type is extracted from line of code"""
        # Arrange
        e = Extractor().datatype_ext

        line = '@'

        # Act

        # Assert
        with self.assertRaises(ValueError) as context:
            e._extract_attribute_datatypes(line)
        self.assertTrue("No data type detected for '@'" in str(context.exception))


if __name__ == '__main__':
    unittest.main(verbosity=2)
