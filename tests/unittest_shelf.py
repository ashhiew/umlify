from unittest import TestCase, main
from shelf import Shelf
import shelve


class ShelfUnitTests(TestCase):
    """Unittests for Shelf"""
    def setUp(self):
        self.shelf_name = "test_shelve"

    def test_write_shelf(self):
        # Arrange
        self.shelf = Shelf("tests/test_class_2.py")
        self.shelf.clear_shelf(self.shelf_name)

        # Act
        self.shelf.write_shelf(self.shelf_name)
        with shelve.open(self.shelf_name) as shelf:
            actual = bool(shelf)

        # Assert
        self.assertTrue(actual)

    def test_write_shelf_if_shelf_name_none(self):
        # Arrange
        self.shelf = Shelf("tests/test_class_2.py")
        self.shelf.clear_shelf(self.shelf_name)

        # Act
        actual = self.shelf.write_shelf()

        # Assert
        self.assertIsNone(actual)

    def test_read_shelf(self):
        # Arrange
        self.shelf = Shelf("tests/test_class_5.py")
        self.shelf.clear_shelf(self.shelf_name)
        self.shelf.write_shelf(self.shelf_name)

        expected = "Class Name: Plant\n"
        expected += "Attribute: plant_height\n"
        expected += "Function: __init__\n"
        expected += "Function: grow_plant\n"
        expected += "-"*20 + "\n"
        expected += "Class Name: Sunflower\n"
        expected += "Function: drop_seed\n"
        expected += "Parent: Plant\n"
        expected += "-"*20 + "\n"
        expected += "Class Name: Orchid\n"
        expected += "Parent: Plant\n"
        expected += "-"*20 + "\n"

        # Act
        actual = self.shelf.read_shelf(self.shelf_name)

        # Assert
        self.assertMultiLineEqual(expected, actual)

    def test_read_shelf_two_dict(self):
        self.shelf = Shelf("tests/test_class_1.py")
        self.shelf.clear_shelf(self.shelf_name)

        expected = "Class Name: DemoClass\n"
        expected += "-" * 20 + "\n"
        expected += "Class Name: Plant\n"
        expected += "Attribute: plant_height\n"
        expected += "Function: __init__\n"
        expected += "Function: grow_plant\n"
        expected += "-" * 20 + "\n"
        expected += "Class Name: Sunflower\n"
        expected += "Function: drop_seed\n"
        expected += "Parent: Plant\n"
        expected += "-" * 20 + "\n"
        expected += "Class Name: Orchid\n"
        expected += "Parent: Plant\n"
        expected += "-" * 20 + "\n"

        # Act
        self.shelf.write_shelf(self.shelf_name)
        self.shelf = Shelf("tests/test_class_5.py")
        self.shelf.write_shelf(self.shelf_name)
        actual = self.shelf.read_shelf(self.shelf_name)

        # Assert
        self.assertEqual(expected, actual)

    def test_read_shelf_exception_thrown(self):
        self.shelf = Shelf("tests/test_class_6.py")
        with self.assertRaises(Exception):
            self.shelf.read_shelf(None)

    def test_clear_shelf(self):
        # Arrange
        shelf = Shelf("tests/test_class_6.py")
        shelf.clear_shelf(self.shelf_name)
        expected = []

        # Act
        with shelve.open(self.shelf_name, 'r') as test_file:
            actual = list(test_file.keys())

        # Assert
        self.assertListEqual(expected, actual)


if __name__ == '__main__':
    main(verbosity=2)
