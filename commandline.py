"""
This module handles the command line input and help messages for Umlify
"""

import sys
from cmd import Cmd
from shelf import Shelf
from database import Database
from umlify_component_viewer import UmlifyComponentViewer


class CommandLine(Cmd):
    """
    CommandLine class that uses Cmd
    Instead of a single command with multiple options (eg `umlify -d ./myclasses/ -o image.png`
    it will have multiple commands to set each value
    """

    def __init__(self, testing=False):
        Cmd.__init__(self)
        self.prompt = "Umlify> "
        self.intro = "Welcome to Umlify. Use \"help\" for help."
        self.input_path = None
        self.output_path = None
        self.allowed_types = ["dot", "png", "pdf"]
        # default output file type .dot (first in allowed types)
        self.output_file_type = self.allowed_types[0]
        self.run = False
        self.cv = None
        self.db = Database()
        self.db.create_connection("uml_components.db")
        if len(sys.argv) > 1 and not testing:
            self.start(sys.argv)
        elif not testing:
            # only run cmdloop if not using commandline arguments
            self.cmdloop()

    def start(self, params):
        """
        >>> cl = CommandLine(True)
        Connected to database
        Database created
        table created for database uml_components.db
        >>> args = "-f tests/test_class_1.py".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "tests/test_class_1.py"
        Running Umlify...
        Quitting Umlify.

        >>> args = "-f tests/test_class_5.py -b".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "tests/test_class_5.py"
        Running Umlify...
        Generating a bar chart
        Quitting Umlify.

        >>> args = "-f tests/test_class_5.py -p Sunflower".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "tests/test_class_5.py"
        Running Umlify...
        Generating a pie chart for component: Sunflower
        Quitting Umlify.

        >>> args = "-f tests/test_class_5.py -p Orchid -b".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "tests/test_class_5.py"
        Running Umlify...
        Generating a bar chart
        Generating a pie chart for component: Orchid
        Quitting Umlify.

        >>> args = "-f".split()
        >>> cl.start([sys.argv[0]] + args)
        Traceback (most recent call last):
            ...
        Exception: You must provide a file name with "-f"

        Usage: start -f <file> [-bc [output_file]|-pc [comp_name] [output_file]]
        :param params: The commandline arguments and inputs
        :return:
        """
        # command help
        if "-h" in params or "-help" in params:
            print("""Generates UML diagrams of Python code

Umlify command usage:
  {file} -f <file> [-h|-b [output_file]|-p [comp_name] [output_file]]

  -f <file> : specifies the Python file to generate diagrams from
      a class diagram is generated when the program is run

  -b [output_file] : generates a bar chart
      using \"output_file\" sets the output file for the bar chart

  -p [comp_name] [output_file] : generates pie charts for all components
      using \"comp_name\" generates a pie chart only for comp_name
      using \"output_file\" sets the output file for the \"comp_name\" pie chart

  -h : displays this help message""".format(file=params[0]))

            return

        # file setting
        if "-f" in params:
            try:
                file = params[params.index("-f") + 1]
            except IndexError:
                raise Exception("You must provide a file name with \"-f\"")
            self.do_file(file)
            self.do_run()
        else:
            print("Please select an input path \"-f <file>\"")
            self.do_quit()
            return

        # bar chart
        if "-b" in params:
            try:
                bc_output_file = params[params.index("-b") + 1]
            except IndexError:
                bc_output_file = None

            if bc_output_file is None or bc_output_file[0] == "-":
                # this is another option, don't use it
                bc_output_file = None

            self.do_bar_chart(bc_output_file)

        if "-p" in params:
            try:
                comp_name = params[params.index("-p") + 1]
            except IndexError:
                comp_name = None
            # This is separate because comp_name can be given without pc_output_file
            try:
                pc_output_file = params[params.index("-p") + 2]
            except IndexError:
                pc_output_file = None

            if comp_name is None or comp_name[0] == "-":
                pc_params = None
            elif pc_output_file is None or pc_output_file[0] == "-":
                pc_params = comp_name
            else:
                pc_params = comp_name + " " + pc_output_file

            self.do_pie_chart(pc_params)

        self.do_quit()

    @staticmethod
    def help_run():
        print("""
Usage: run
Runs Umlify with the current settings, use other commands to change them
:return:
""")

    def do_run(self, line=None):
        """
        >>> cl = CommandLine(True)
        Connected to database
        Database created
        table created for database uml_components.db
        >>> cl.do_run()
        Please select an input path with "file" or "directory"

        >>> cl.do_file("tests/test_class_1.py")
        Input file set to "tests/test_class_1.py"
        >>> cl.do_run()
        Running Umlify...
        """

        if not self.input_path:
            print("Please select an input path with \"file\" or \"directory\"")
            return
        self.cv = UmlifyComponentViewer(self.input_path, self.output_path)
        self.cv.generate_class_diagram()
        self.run = True
        print("Running Umlify...")

    def _check_input(self):
        """
        >>> cl = CommandLine(True)
        Connected to database
        Database created
        table created for database uml_components.db
        >>> cl._check_input()
        Please select an input path with \"file\" or \"directory\"
        False
        >>> cl.do_file("tests/test_class_1.py")
        Input file set to "tests/test_class_1.py"
        >>> cl._check_input()
        True

        Check if an input has been set, if not then return a message and false
        :return: True if an input file or directory has been set, False if not
        """
        # removes duplication of this message in code
        if not self.input_path:
            print("Please select an input path with \"file\" or \"directory\"")
            return False
        return True

    def _check_run(self):
        """
        Check if the Umlify has been run, if not then return a message and false
        :return: True if run has been used, False if not
        """
        # removes duplication of this message in code
        if not self.run:
            print("Please use \"run\" before trying to create a chart")
            return False
        return True

    @staticmethod
    def help_bar_chart():
        print("""
Usage: bar_chart [output_file_name]
Generate a bar chart about classes
:param output_file_name: the file name for the chart to output to
:return:
""")

    def do_bar_chart(self, output_file_name=None):
        """
        >>> cl = CommandLine(True)
        Connected to database
        Database created
        table created for database uml_components.db
        >>> cl.do_bar_chart()
        Please use "run" before trying to create a chart

        >>> cl.do_file("tests/test_class_1.py")
        Input file set to "tests/test_class_1.py"
        >>> cl.do_run()
        Running Umlify...
        >>> cl.do_bar_chart()
        Generating a bar chart
        """
        if self._check_run():
            if not output_file_name:
                output_file_name = None
            self.cv.generate_bar_chart(output_file_name)
            print("Generating a bar chart")

    @staticmethod
    def help_pie_chart():
        print("""
Usage: pie_chart [comp_name] [output_file_name]
Makes a pie chart about a class. Give no input for pie charts of every component
:param params: the input which includes optional values comp_name and output_file_name
comp_name the name of the component to make a chart of
output_file_name the file name for the chart to output to
:return:
""")

    def do_pie_chart(self, params=None):
        """
        >>> cl = CommandLine(True)
        Connected to database
        Database created
        table created for database uml_components.db
        >>> cl.do_pie_chart()
        Please use "run" before trying to create a chart

        >>> cl.do_file("tests/test_class_5.py")
        Input file set to "tests/test_class_5.py"
        >>> cl.do_run()
        Running Umlify...
        >>> cl.do_pie_chart()
        Generating pie charts for all components
        >>> cl.do_pie_chart("Sunflower")
        Generating a pie chart for component: Sunflower
        """
        if not self._check_run():
            return

        if not params:
            print("Generating pie charts for all components")
            self.cv.generate_pie_charts()
            return
        if " " in params:
            # split params into component name and output file
            comp_name, output_file_name = params.split(' ')
        else:
            comp_name = params
            output_file_name = None

        # example of comp_name: "Herbivore"
        print("Generating a pie chart for component: {comp_name}".format(comp_name=comp_name))
        self.cv.generate_pie_chart(comp_name, output_file_name)

    # def do_validate(self, line):
    #    """
    #    Validates a file without passing it to be drawn
    #    :return: None
    #    """
    #
    #    pass

    @staticmethod
    def help_file():
        print("""
Usage: file <file>
Selects a file as input to Umlify
:param file: the name of the file to be used as input
:return: None
""")

    def do_file(self, file):
        """
        >>> cl = CommandLine(True)
        Connected to database
        Database created
        table created for database uml_components.db
        >>> cl.do_file(None)
        Please enter a file to use as input
        >>> cl.do_file("tests/test_class_1.py")
        Input file set to "tests/test_class_1.py"
        """
        if file:
            self.input_path = file
            print("Input file set to \"{file}\"".format(file=file))
        else:
            print("Please enter a file to use as input")

    @staticmethod
    def help_directory():
        print("""
Usage: directory <directory>
Selects a directory as input to Umlify
:param directory: the name of the directory holding files to be used as input
:return: None
""")

    def do_directory(self, directory):

        if directory:
            self.input_path = directory
        else:
            print("Please enter a directory to use as input")

    # @staticmethod
    # def help_location():
    #    print("""
    # Usage: location <location>
    # Selects a destination location for the output files
    # :param location:
    # :return: None
    # """)

    # def do_location(self, location):
    #    if not location:
    #        print("Please enter a location for output files to be saved")

    def help_type(self):
        print("""
Usage: type <file_type>
Sets the output file type
Allowed types: {file_types}
:param file_type: a string representing a file extension
:return: None
""".format(file_types=', '.join(self.allowed_types)))

    def do_type(self, file_type):
        """
        >>> cl = CommandLine(True)
        Connected to database
        Database created
        table created for database uml_components.db
        >>> cl.do_type(None)
        Please enter a file type
        >>> cl.do_type("png")
        Output file type has been set to png
        >>> cl.do_type("png")
        The output file type is already png
        >>> cl.do_type("pdf")
        Output file type has been set to pdf
        >>> cl.do_type(".png")
        Output file type has been set to png
        >>> cl.do_type(".DOT")
        Output file type has been set to dot
        >>> cl.do_type("invalid_type")
        That file type is not supported, please pick one from: dot, png, pdf
        """
        if not file_type:
            # no file type was provided
            print("Please enter a file type")
            return

        file_type = file_type.lower()  # convert to all lowercase

        # remove leading . if given (eg .png instead of png)
        if file_type[0] == ".":
            file_type = file_type[1:]

        if file_type == self.output_file_type:
            print("The output file type is already {file_type}".format(file_type=file_type))
        elif file_type in self.allowed_types:
            self.output_file_type = file_type
            print("Output file type has been set to {file_type}".format(file_type=file_type))
        else:
            # input provided was not in the list of allowed types
            print("That file type is not supported, please pick one from: {file_types}".format(
                file_types=', '.join(self.allowed_types)))

    @staticmethod
    def help_shelf():
        print("""
        Writes and reads Component objects extracted from the currently selected file to [filename].shelve

        Syntax: shelf [flag] OR s [flag]
        shelf -w: writes the class components of selected file to [filename].shelve
                  User will be prompted to give a name for shelve file
        shelf -r: reads the contents of shelve file
                  User will be prompted to input name of shelve file
        :param flag: -w, -r
        :return: By default, invalid cmd
        """)

    def do_shelf(self, flag):
        if not self._check_input():
            return

        shelf = Shelf(self.input_path)

        try:
            if flag == '-w':
                try:
                    shelf_name = input("Enter a name for shelve file: ")
                    if not shelf_name:
                        raise ValueError("You did not enter a name")
                    else:
                        print(shelf.write_shelf(shelf_name))
                except Exception:
                    raise Exception("You did not enter a name")

            elif flag == '-r':
                try:
                    shelf_name = input("Enter the name of shelve file you would like to open: ")
                    if not shelf_name:
                        raise ValueError("You did not enter a name")
                    else:
                        print(shelf.read_shelf(shelf_name))
                except Exception:
                    raise Exception("You did not enter a name")
            else:
                raise Exception("Not a valid flag")
        except Exception as err:
            print("The exception is: ", err)
        return

    @staticmethod
    def help_database():
        print("""
        Save and read class component data with a database. Stores component object as a pickle file.

        Syntax: database [flag] OR db [flag]
        db: Displays the name of current database connected and the number of pickled files stored
        db -a: displays all data and the number of components stored in the database
        db -i: inserts the class components of currently selected file to the database
        db -r: removes the class components of currently selected file from database
        db -v: displays the class components of a certain filename within the database
               User will be prompted to enter a filename
        db -n: create a new connection with a new database
        db -c: create a new table called classes into the database
        db -d: drops the table from database

        :param flag: -a, -i, -v, -n, -d
        :return: db values
        """)

    def do_database(self, flag=""):
        if not self._check_input():
            return

        try:
            if flag == "-a":
                print(self.db.get_all())
            elif flag == "-i":
                self.db.insert_data(self.input_path)
            elif flag == "-r":
                self.db.remove_data(self.input_path)
            elif flag == "-v":
                filename = input("Enter a filename you would like to see the classes of: ")
                print(self.db.get_specific(filename))
            elif flag == "-n":
                db_name = input("Enter a database name: ")
                self.db.create_connection(db_name)
            elif flag == "-c":
                confirmation = input("Do you want to create a new table for the database? (Y/N)")
                if confirmation is "Y" or confirmation is "":
                    self.db.create_table()
            elif flag == "-d":
                confirmation = input("Do you want to drop table from database? (Y/N):")
                if confirmation is "Y" or confirmation is "":
                    self.db.drop_table()
            elif flag == "":
                print(self.db.get_db_info())
            else:
                raise Exception("Not a valid flag")
        except Exception as err:
            print("The exception is: ", err)
        return

    @staticmethod
    def help_quit():
        print("""
                Usage: quit
                Quit Umlify
                :return: True
                """)

    @staticmethod
    def do_quit():
        """
        >>> cl = CommandLine(True)
        Connected to database
        Database created
        table created for database uml_components.db
        >>> cl.do_quit()
        Quitting Umlify.
        True
        """
        print("Quitting Umlify.")
        return True

    # command aliases
    do_r = do_run
    help_r = help_run
    do_b = do_bar_chart
    help_b = help_bar_chart
    do_p = do_pie_chart
    help_p = help_pie_chart
    # do_v = do_validate
    do_f = do_file
    help_f = help_file
    do_d = do_directory
    help_d = help_directory
    # do_l = do_location
    # help_l = help_location
    do_t = do_type
    help_t = help_type
    do_s = do_shelf
    help_s = help_shelf
    do_db = do_database
    help_db = help_database
    do_q = do_quit
    help_q = help_quit


if __name__ == "__main__":
    command_line = CommandLine()
    # import doctest

    # doctest.testmod(verbose=True)
